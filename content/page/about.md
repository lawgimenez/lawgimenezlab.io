---
title: About me
subtitle: I'm Lawrence Gimenez, I could say I am an experienced mobile app developer.
comments: false
date: 2018-07-21
---

I'm Lawrence Gimenez, I could say I am an experienced mobile app developer. I have been developing for the Android platform since 2011, and for the iOS platform since Swift beta went out.


I have been part of several local and international clients throughout my career. 